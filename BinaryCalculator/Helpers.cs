﻿namespace BinaryCalculator
{
    public class Helpers
    {
        public string GetBinary(string input)
        {
            string output = "";
            foreach (var letter in input)
            {
                if (letter == '0' || letter == '1')
                {
                    output += letter;
                }
            }

            return output;
        }
        public string Revert(string input)
        {
            string output = "";
            for (int i = input.Length - 1; i >= 0; i--)
            {
                output += input[i];
            }

            return output;
        }
    }
}