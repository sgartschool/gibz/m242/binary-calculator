﻿using System;

namespace BinaryCalculator
{
    public class Calculator
    {
        // Result string
            string reverseBinaryResult = "";
            // Rest value
            
            
        public string Sum(string input1, string input2)
        {
            string output = "";
            // Has rest
            bool rest = false;
            // Longest value for for-loop
            int length = input1.Length < input2.Length ? input2.Length : input1.Length;
            for (int i = 0; i < length; i++)
            {
                // Prevent index out of bounds exception
                var value1 = i < input1.Length ? input1[i] : '0';
                var value2 = i < input2.Length ? input2[i] : '0';
                
                // Calculate
                if (value1 == '1')
                {
                    if (value2 == '1')
                    {
                        if (rest)
                        {
                            output += '1';
                            Console.WriteLine("1 + 1 + 1 = 1 | 1");
                        }
                        else
                        {
                            output += '0';
                            Console.WriteLine("1 + 1 + 0 = 0 | 1");
                        }
                        rest = true;
                    }
                    else
                    {
                        if (rest)
                        {
                            output += '0';
                            Console.WriteLine("1 + 0 + 1 = 0 | 1");
                        }
                        else
                        {
                            output += '1';
                            Console.WriteLine("1 + 0 + 0 = 1 | 0");

                        }
                    }
                }
                else
                {
                    if (value2 == '1')
                    {
                        if (rest)
                        {
                            output += '0';
                            Console.WriteLine("0 + 1 + 1 = 0 | 1");
                        }
                        else
                        {
                            output += '1';
                            Console.WriteLine("0 + 1 + 0 = 1 | 0");
                        }
                    }
                    else
                    {
                        if (rest)
                        {
                            output += '1';
                            rest = false;
                            Console.WriteLine("0 + 0 + 1 = 1 | 0");
                        }
                        else
                        {
                            output += '0';
                            Console.WriteLine("0 + 0 + 0 = 0 | 0");
                        }
                    }
                }
            }
            
            // Add rest if existing
            output += rest ? "1" : "";

            return output;
        }
    }
}