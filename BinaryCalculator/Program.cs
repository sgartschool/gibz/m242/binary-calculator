﻿using System;
using System.Runtime.InteropServices;

namespace BinaryCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Required classes
            Helpers helpers = new Helpers();
            Calculator calculator = new Calculator();
            
            // Promt for first value
            Console.WriteLine("Enter first binary value below:");
            // Read first value and safe it as variable
            string consoleInput1 = Console.ReadLine();
            // Remove non-binary numbers or letters
            string binaryNumber1 = helpers.GetBinary(consoleInput1);
            // Repeat cleaned value for user
            Console.WriteLine($"Value 1: {binaryNumber1}");
            // Paragraph to make output cleaner
            Console.WriteLine();
            
            // Promt for second value
            Console.WriteLine("Enter second binary value below:");
            // Read first value and safe it as variable
            string consoleInput2 = Console.ReadLine();
            // Remove non-binary numbers or letters
            string binaryNumber2 = helpers.GetBinary(consoleInput2);
            // Repeat cleaned value for user
            Console.WriteLine($"Value 2: {binaryNumber2}");
            // Paragraph to make output cleaner
            Console.WriteLine();
            
            // Line to make output cleaner
            Console.WriteLine("-----------------");
            
            // Invert values to calculate propertly
            string reverseBinary1 = helpers.Revert(binaryNumber1);
            string reverseBinary2 = helpers.Revert(binaryNumber2);

            // Calculator
            string revResult = calculator.Sum(reverseBinary1, reverseBinary2);
            
            // Re-Revert result
            string binaryResult = helpers.Revert(revResult);
            
            // Line to make output cleaner
            Console.WriteLine("-----------------");
            // Output result
            Console.WriteLine($"{binaryNumber1} + {binaryNumber2} = {binaryResult}");
        }
    }
}